module gitlab.com/mergetb/tech/sled

go 1.15

require (
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/gojuno/minimock v1.9.2 // indirect
	github.com/google/goexpect v0.0.0-20210430020637-ab937bf7fd6f // indirect
	github.com/hugelgupf/socketpair v0.0.0-20190730060125-05d35a94e714 // indirect
	github.com/insomniacslk/dhcp v0.0.0-20210120172423-cc9239ac6294
	github.com/mdlayher/ethernet v0.0.0-20190606142754-0394541c37b7 // indirect
	github.com/mdlayher/raw v0.0.0-20191009151244-50f2db8cc065 // indirect
	github.com/minio/minio-go/v7 v7.0.9-0.20210210235136-83423dddb072
	github.com/rekby/gpt v0.0.0-20200614112001-7da10aec5566 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/u-root/u-root v7.0.0+incompatible
	github.com/vishvananda/netlink v1.1.1-0.20201029203352-d40f9887b852
	gitlab.com/mergetb/api v0.0.0-20210614222316-99a96178dec6
	gitlab.com/mergetb/tech/rtnl v0.1.9
	gitlab.com/mergetb/tech/shared/storage v0.0.0-20220709055424-6cc306a1809b
	go.etcd.io/etcd v0.5.0-alpha.5.0.20200910180754-dd1b699fc489 // indirect
	google.golang.org/grpc v1.47.0
	pack.ag/tftp v1.0.0 // indirect
)

replace github.com/insomniacslk/dhcp => github.com/insomniacslk/dhcp v0.0.0-20200420235442-ed3125c2efe7

replace google.golang.org/grpc => google.golang.org/grpc v1.27.0
